



$(document).ready(function () {

    var spacesTaken = [false, false, false, false, false, false, false, false, false];
    var takenBy = [];
    var bestMove;
    
    var difficulty = 3;
    var color = "Blue";

    
    function game() {
    }

    game.prototype.toArray = function (space, user) {
        spacesTaken[space] = true;
        if (user) {
            takenBy[space] = "O";
        }
        else {
            takenBy[space] = "X";
        }
    };

    
    game.prototype.draw = function (space, isUser) {
        
        if (isUser) {
            switch (space) {
                case 1:

                    $("#square1").attr("src", "images/img"+ color +"X.jpg");

                    break;
                case 2:

                    $("#square2").attr("src", "images/img" + color + "X.jpg");

                    break;
                case 3:

                    $("#square3").attr("src", "images/img" + color + "X.jpg");

                    break;
                case 4:

                    $("#square4").attr("src", "images/img" + color + "X.jpg");

                    break;
                case 5:

                    $("#square5").attr("src", "images/img" + color + "X.jpg");

                    break;
                case 6:

                    $("#square6").attr("src", "images/img" + color + "X.jpg");

                    break;
                case 7:

                    $("#square7").attr("src", "images/img" + color + "X.jpg");

                    break;
                case 8:

                    $("#square8").attr("src", "images/img" + color + "X.jpg");

                    break;
                case 9:

                    $("#square9").attr("src", "images/img" + color + "X.jpg");

                    break;
            }
        }

        if (!isUser) {
            switch (space) {
                case 1:

                    $("#square1").attr("src", "images/imgGreenO.jpg");
                    break;
                case 2:

                    $("#square2").attr("src", "images/imgGreenO.jpg");

                    break;
                case 3:

                    $("#square3").attr("src", "images/imgGreenO.jpg");

                    break;
                case 4:

                    $("#square4").attr("src", "images/imgGreenO.jpg");

                    break;
                case 5:

                    $("#square5").attr("src", "images/imgGreenO.jpg");

                    break;
                case 6:

                    $("#square6").attr("src", "images/imgGreenO.jpg");

                    break;
                case 7:

                    $("#square7").attr("src", "images/imgGreenO.jpg");

                    break;
                case 8:

                    $("#square8").attr("src", "images/imgGreenO.jpg");

                    break;
                case 9:

                    $("#square9").attr("src", "images/imgGreenO.jpg");

                    break;
            }
        }
    };

    game.prototype.checkWin = function () {

        if (takenBy[0] === "X" && takenBy[1] === "X" && takenBy[2] === "X") {
            return "X";
        }
        else if (takenBy[3] === "X" && takenBy[4] === "X" && takenBy[5] === "X") {
            return "X";
        }
        else if (takenBy[6] === "X" && takenBy[7] === "X" && takenBy[8] === "X") {
            return "X";
        }

        if (takenBy[0] === "X" && takenBy[3] === "X" && takenBy[6] === "X") {
            return "X";
        }
        else if (takenBy[1] === "X" && takenBy[4] === "X" && takenBy[7] === "X") {
            return "X";
        }
        else if (takenBy[2] === "X" && takenBy[5] === "X" && takenBy[8] === "X") {
            return "X";
        }

        if (takenBy[0] === "X" && takenBy[4] === "X" && takenBy[8] === "X") {
            return "X";
        }
        else if (takenBy[2] === "X" && takenBy[4] === "X" && takenBy[6] === "X") {
            return "X";
        }

        if (takenBy[0] === "O" && takenBy[1] === "O" && takenBy[2] === "O") {
            return "O";
        }
        else if (takenBy[3] === "O" && takenBy[4] === "O" && takenBy[5] === "O") {
            return "O";
        }
        else if (takenBy[6] === "O" && takenBy[7] === "O" && takenBy[8] === "O") {
            return "O";
        }

       if (takenBy[0] === "O" && takenBy[3] === "O" && takenBy[6] === "O") {
            return "O";
        }
        else if (takenBy[1] === "O" && takenBy[4] === "O" && takenBy[7] === "O") {
            return "O";
        }
        else if (takenBy[2] === "O" && takenBy[5] === "O" && takenBy[8] === "O") {
            return "O";
        }

       if (takenBy[0] === "O" && takenBy[4] === "O" && takenBy[8] === "O") {
            return "O";
        }
        else if (takenBy[2] === "O" && takenBy[4] === "O" && takenBy[6] === "O") {
            return "O";
        }

        var isATie = true;
       for (var i = 0; i < spacesTaken.length; i++) {
            if (spacesTaken[i] === false) {
                isATie = false;
            }
        }
        if (isATie) {
            return "tie";
        }

        return "neither";
    };

    var match = new game();

     game.prototype.doMove = function (space, isUser) {

        if (isUser) {
            match.draw(space, true);
            match.toArray(space - 1, true);
            match.check();
            match.cpuMove();
        }
        else {
            match.draw(space, false);
            match.toArray(space - 1, false);
        }

       match.check();
    };

    game.prototype.exit = function () {
        spacesTaken = [false, false, false, false, false, false, false, false, false];
        takenBy = [];
        location.reload();
    };

   game.prototype.goFirstOrSecond = function () {
        var first = prompt("To be first Type 1 : Else the CPU will move first. You are X");
        if (first !== 1) {
            match.cpuMove();
        }
        difficulty = prompt("Please enter 1 for easy, 2 for medium, and 3 for hard.");
        color = prompt("type your color: Aqua, Blue, Green, Magenta, Plain, Red, White, or Yellow");
           var isValidColor = false;
        if (color === "Blue" || color === "Aqua" || color === "Green" || color === "Magenta" || color === "Plain" || color === "Red" || color === "White" || color === "Yellow") {
            isValidColor = true;
        }

        while (!isValidColor) {
            color = prompt("Please onnly choose from the options provieded (Caps included) : Aqua, Blue, Green, Magenta, Plain, Red, White, or Yellow");
            if (color === "Blue" || color === "Aqua" || color === "Green" || color === "Magenta" || color === "Plain" || color === "Red" || color === "White" || color === "Yellow") {
                isValidColor = true;
            }
        };
   };

    game.prototype.check = function () {

        var won = match.checkWin();
        var restart = "No";

        if (won === "X" || won === "x") {
            alert("The computer has won!");
            restart = prompt("Play again? Yes/ No");
            if (restart === "Yes") {
                match.exit();
            } else {
                spacesTaken = [false, false, false, false, false, false, false, false, false];
                takenBy = [];
                return;
            }
            
        }
        else if (won === "O" || won === "o") {
            alert("You have won!");
            restart = prompt("Play again? Yes/ No");
            if (restart === "Yes") {
                match.exit();
            } else {
                spacesTaken = [false, false, false, false, false, false, false, false, false];
                takenBy = [];
                return;
            }
            
        }
        else if (won === "tie") {
            alert("The game is a tie!");
            restart = prompt("Play again? Yes/ No");
            if (restart === "Yes") {
                match.exit();
            } else {
                spacesTaken = [false, false, false, false, false, false, false, false, false];
                takenBy = [];
                return;
            }
        }
    };

    
    game.prototype.cpuMove = function () {

        for (var i = 0; i < spacesTaken.length; i++) {
            if (spacesTaken[i] === false) {
                bestMove = i + 1;
            }
        }

        
        if (difficulty > 2) {
           if (spacesTaken[6] === false && spacesTaken[8] === false) {
                bestMove = 7;
            }
        }

        
        if (spacesTaken[4] === false) {
            bestMove = 5;
        }

        
        if (difficulty > 1) {
            var tempMove;
            tempMove = 10;

            tempMove = match.canWin("O", true);
            if (tempMove !== 10) {
                bestMove = tempMove + 1;
            }

            tempMove = match.canWin("X", false);
            if (tempMove !== 10) {
                bestMove = tempMove + 1;
            }
        }

        match.draw(bestMove, false);
        match.doMove(bestMove, false);
    };

    game.prototype.canWin = function (winningChar, isUser) {
        var winPossible;
        var winningMove = 10;
        for (var i = 0; i < 8; i++) {
            if (spacesTaken[i] === false) {
                var priorValue = takenBy[i];
                match.toArray(i, isUser);
                winPossible = match.checkWin();
                if (winPossible === winningChar) {
                    i = winningMove;
                    spacesTaken[i] = false;
                    takenBy[i] = priorValue;
                    return i;
                }
                
                spacesTaken[i] = false;
                takenBy[i] = priorValue;
            }
        }
        return 10;
    };

   
    match.goFirstOrSecond();

    $("#square1").click(function () {
        match.doMove(1, true);
    });
    $("#square2").click(function () {
        match.doMove(2, true);
    });
    $("#square3").click(function () {
        match.doMove(3, true);
    });
    $("#square4").click(function () {
        match.doMove(4, true);
    });
    $("#square5").click(function () {
        match.doMove(5, true);
    });
    $("#square6").click(function () {
        match.doMove(6, true);
    });
    $("#square7").click(function () {
        match.doMove(7, true);
    });
    $("#square8").click(function () {
        match.doMove(8, true);
    });
    $("#square9").click(function () {
        match.doMove(9, true);
    });
});